package caece.siete.cinco;

public abstract class Numero {

	public abstract int suma(Numero num);
	
	public abstract int producto(Numero num);
}
