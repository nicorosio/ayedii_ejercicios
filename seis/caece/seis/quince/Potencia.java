package caece.seis.quince;

public class Potencia {

	private float base;
	
	private int exponente;

	public Potencia(float base, int exponente) {
		super();
		this.base = base;
		this.exponente = exponente;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public int getExponente() {
		return exponente;
	}

	public void setExponente(int exponente) {
		this.exponente = exponente;
	}
	
	public float evaluar() {
		int aux = this.exponente;
		return evaluarRec(aux);
	}
	
	public float evaluarRec(int exponente) {
		if ( exponente > 0 ) {
			return this.base * evaluarRec(exponente--);
		}
		return 1;
	}
	
}
