package caece.siete.tres;

public class Libro extends Publicacion {

	private int cantPag;
	
	private int anioPublicacion;

	public Libro(String titulo, double precio, int cantPag, int anioPublicacion) {
		super(titulo, precio);
		this.cantPag = cantPag;
		this.anioPublicacion = anioPublicacion;
	}

	public int getCantPag() {
		return cantPag;
	}

	public void setCantPag(int cantPag) {
		this.cantPag = cantPag;
	}

	public int getAnioPublicacion() {
		return anioPublicacion;
	}

	public void setAnioPublicacion(int anioPublicacion) {
		this.anioPublicacion = anioPublicacion;
	}
	
	
	
}
