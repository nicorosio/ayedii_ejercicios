package caece.seis.diez;

public class Complejo {

	private float real;
	
	private float imaginaria;
	
	public Complejo() {
		
	}

	public Complejo(float real, float imaginaria) {
		super();
		this.real = real;
		this.imaginaria = imaginaria;
	}
	
	public void suma(int real, int imaginaria) {
		this.real += real;
		this.imaginaria += imaginaria;
	}
	
	public void suma(float real, float imaginaria) {
		this.real += real;
		this.imaginaria += imaginaria;
	}
	
	public Complejo suma(Complejo valUno, Complejo valDos) {
		Complejo result = new Complejo();
		result.setReal( valUno.getReal() + valDos.getReal());
		result.setImaginaria( valUno.getImaginaria() + valDos.getImaginaria() );
		return result;
	}

	public float getReal() {
		return real;
	}

	public void setReal(float real) {
		this.real = real;
	}

	public float getImaginaria() {
		return imaginaria;
	}

	public void setImaginaria(float imaginaria) {
		this.imaginaria = imaginaria;
	}

	
	
	
}
