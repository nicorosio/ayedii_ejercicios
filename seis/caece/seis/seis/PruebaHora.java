package caece.seis.seis;

public class PruebaHora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hora h1 = new Hora(10,15,20);
		Hora h2 = new Hora(12,17,22);
		Hora h3;
		
		System.out.println("INICIO");
		System.out.println(h1.verHora());
		System.out.println(h2.verHora());
		
		h3 = h1.sumarHoras(h2);
		
		System.out.println("SUMAR");
		System.out.println(h3.verHora());
		
		h1.adicionarHoras(h2);
		
		System.out.println("ADICIONAR");
		System.out.println(h1.verHora());
	}

}
