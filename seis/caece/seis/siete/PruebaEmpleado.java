package caece.seis.siete;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PruebaEmpleado {

	public static void cargarEmpleados(List<Empleado> empleados) {
		String[] names = {"Jose","Juan","Carlos","Martin"};
		for (int i = 0; i < names.length; i++) {
			Empleado auxEmp = new Empleado(i, names[i]);
			empleados.add(auxEmp);
		}
		
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Empleado> empleados = new ArrayList<Empleado>();
		cargarEmpleados(empleados);
		for (Empleado empleado : empleados) {
			System.out.println(empleado.verDatos());
		}
	}
	
	
}
