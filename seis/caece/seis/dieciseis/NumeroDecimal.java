package caece.seis.dieciseis;

public class NumeroDecimal {

	private int value;

	public NumeroDecimal(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public NumeroBinario aBinario() {
		return new NumeroBinario(Integer.toBinaryString(this.value));
	}
	
}
