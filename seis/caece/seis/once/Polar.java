package caece.seis.once;

import caece.seis.ocho.Punto;

public class Polar {

	private double radio;
	
	private double angulo;
	
	public Polar(double radio, double angulo) {
		super();
		this.radio = radio;
		this.angulo = angulo;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}

	public Punto polarToPunto() {
		Punto pto = new Punto();
		pto.setX( (float)(this.radio * Math.cos(this.angulo)) );
		pto.setY( (float)(this.radio * Math.sin(this.angulo)) );
		return pto;
	}
	
}
