package caece.siete.dos;

public class Estudiante extends Persona {
	
	private String carrera;
	
	private int legajo;

	public Estudiante(String nombre, String apellido, String genero, int edad, String carrera, int legajo) {
		super(nombre, apellido, genero, edad);
		this.carrera = carrera;
		this.legajo = legajo;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	

}
