package caece.siete.cinco;

public class Fraccion extends Numero {

	private int numerador;
	
	private int denominador;

	public Fraccion(int numerador, int denominador) {
		super();
		this.numerador = numerador;
		this.denominador = denominador;
	}

	public int getNumerador() {
		return numerador;
	}

	public void setNumerador(int numerador) {
		this.numerador = numerador;
	}

	public int getDenominador() {
		return denominador;
	}

	public void setDenominador(int denominador) {
		this.denominador = denominador;
	}
	
	public int suma(Numero num) {
		return 1;
	}
	
	public int producto(Numero num) {
		return 1;
	}
	
}
