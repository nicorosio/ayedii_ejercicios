package caece.siete.dos;

public class Empleado extends Persona {
	
	private int legajo;
	
	private String area;
	
	private double sueldo;

	public Empleado(String nombre, String apellido, String genero, int edad, int legajo, String area, double sueldo) {
		super(nombre, apellido, genero, edad);
		this.legajo = legajo;
		this.area = area;
		this.sueldo = sueldo;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}	

}
