package caece.siete.tres;

public class Publicacion {

	private String titulo;
	
	private double precio;

	public Publicacion(String titulo, double precio) {
		super();
		this.titulo = titulo;
		this.precio = precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
	
	
}
