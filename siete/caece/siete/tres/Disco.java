package caece.siete.tres;

public class Disco extends Publicacion {

	public int duracion;

	
	public Disco(String titulo, double precio, int duracion) {
		super(titulo, precio);
		this.duracion = duracion;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	
	
}
