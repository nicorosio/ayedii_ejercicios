package caece.seis.dieciseis;

public class NumeroBinario {

	private String value;

	public NumeroBinario(String value) {
		super();
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public NumeroDecimal aDecimal() {
	    char[] numeros = this.value.toCharArray();
	    int result = 0;
	    for(int i = numeros.length - 1; i >=0 ; i--) {
	    	if(numeros[i] == '1') {
	    		result += Math.pow(2, (numeros.length-i - 1));	    		
	    	}	    	
	    }
	    return new NumeroDecimal(result);
	}
	
}
