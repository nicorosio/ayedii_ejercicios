package caece.seis.seis;

public class Hora {
	private int horas;
	private int minutos;
	private int segundos;
	
	public Hora() {
		//Los valores int en java se inizializan en 0 por default
	}
	
	public Hora(int horas, int minutos, int segundos) {
		this.horas = horas;
		this.minutos = minutos;
		this.segundos = segundos;
	}
	
	public String verHora() {
		return frmtTime(horas) + ":" + frmtTime(minutos) + ":" + frmtTime(segundos);
	}
	
	private String frmtTime(int timeValue) {
		if (timeValue < 10) {
			return "0" + Integer.toString(timeValue);
		}
		return Integer.toString(timeValue);
	}
	
	public Hora sumarHoras(Hora hsSumar) {
		Hora result = new Hora();
		result.setHora(this.getHoras() + hsSumar.getHoras());
		result.setMinutos(this.getMinutos() + hsSumar.getMinutos());
		result.setSegundos(this.getSegundos() + hsSumar.getSegundos());
		return result;
	}
	
	public void adicionarHoras(Hora hsSumar) {
		this.setHora(this.getHoras() + hsSumar.getHoras());
		this.setMinutos(this.getMinutos() + hsSumar.getMinutos());
		this.setSegundos(this.getSegundos() + hsSumar.getSegundos());
	}
	
	public int getHoras() {
		return horas;
	}

	public void setHora(int horas) {
		this.horas = horas;
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}

	public int getSegundos() {
		return segundos;
	}

	public void setSegundos(int segundos) {
		this.segundos = segundos;
	}

	
	
}
