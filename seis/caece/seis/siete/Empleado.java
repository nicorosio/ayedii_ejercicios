package caece.seis.siete;

public class Empleado {
	
	int numero;
	String nombre;
	
	public Empleado() {
		super();
	}
	public Empleado(int numero, String nombre) {
		super();
		this.numero = numero;
		this.nombre = nombre;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String verDatos() {
		return Integer.toString(this.numero) + " - " + this.nombre;
	}
}
