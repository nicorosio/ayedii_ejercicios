package caece.seis.uno;

public class Lamparita {
	private boolean encendida;
	
	public Lamparita() {
		this.encendida = false;
	}
	
	public void encender() {
		this.encendida = true;
	}
	
	public void apagar() {
		this.encendida = false;
	}
	
	public boolean estaEncendida() {
		return this.encendida;
	}
	
}
